package pf.upf.proto.rattack.providers;

import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class ParcelPropertyLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof Entry) {
			Entry<String, Set<String>> e = (Entry<String, Set<String>>) element;
			switch (columnIndex) {
			case 0:
				return e.getKey();
			default:
				return e.getValue().toString();
			}
		}

		return null;
	}

}
