package pf.upf.proto.rattack.providers;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.zest.core.viewers.IGraphEntityRelationshipContentProvider;

import pf.upf.proto.rattack.model.Parcel;

public class ParcelGraphContentProvider extends ArrayContentProvider implements
		IGraphEntityRelationshipContentProvider {

	@Override
	public Object[] getRelationships(Object source, Object dest) {
		if (source instanceof Parcel && dest instanceof Parcel) {
			Parcel p1 = (Parcel) source;
			Parcel p2 = (Parcel) dest;
			if (p1.hasNeighbourRelationship(p2))
				return new Object[] { p1.getNeighbourRelationship(p2) };
		}
		return null;
	}

}