package pf.upf.proto.rattack.providers;

import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class CollusionLabelProvider extends LabelProvider implements
		ITableLabelProvider {

	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof Entry) {
			Entry<Set<String>, Set<Set<String>>> e = (Entry<Set<String>, Set<Set<String>>>) element;
			switch (columnIndex) {
			case 0:
				return e.getKey().toString();
			default:
				return e.getValue().toString();
			}
		}
		return null;
	}

	

}
