package pf.upf.proto.rattack.providers;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.zest.core.viewers.EntityConnectionData;

import pf.upf.proto.rattack.model.Neighbour;
import pf.upf.proto.rattack.model.Parcel;

public class ParcelGraphLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof Parcel) {
			Parcel myNode = (Parcel) element;
			return myNode.getName();
		}
		// Not called with the IGraphEntityContentProvider
		if (element instanceof Neighbour) {
			Neighbour myConnection = (Neighbour) element;
			return myConnection.getLabel();
		}

		if (element instanceof EntityConnectionData)
			return "";
		throw new RuntimeException("Wrong type: "
				+ element.getClass().toString());
	}
}