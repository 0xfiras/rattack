package pf.upf.proto.rattack.commands;

import java.io.FileReader;
import java.io.IOException;

import javax.swing.filechooser.FileSystemView;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import pf.upf.proto.rattack.model.ParcelContentProvider;
import pf.upf.proto.rattack.views.CollusionView;
import pf.upf.proto.rattack.views.ParcelView;

import au.com.bytecode.opencsv.CSVReader;

public class OpenGraphFileCommand extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getShell();
		FileDialog fileDialog = new FileDialog(shell, SWT.OPEN);

		fileDialog.setFilterNames(new String[] { "Graph Files (*.g)" });
		fileDialog.setFilterExtensions(new String[] { "*.g" });
		fileDialog.setText("Select file..");
		fileDialog.setFilterPath(FileSystemView.getFileSystemView()
				.getHomeDirectory().toString());
		final String directory = fileDialog.open();
		Job job = new Job("Opening Graph File") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				Display.getDefault().syncExec(new Runnable() {
					@Override
					public void run() {
						CSVReader reader;
						try {
							reader = new CSVReader(new FileReader(directory));
							String[] nextLine;
							int x, y;
							x = y = Integer.MIN_VALUE;
							// Read x and y

							if ((nextLine = reader.readNext()) != null)
								if (nextLine.length == 2) {
									x = Integer.parseInt(nextLine[0]);
									y = Integer.parseInt(nextLine[1]);
								}

							if (x != Integer.MIN_VALUE
									&& y != Integer.MIN_VALUE) {
								// read nodes
								ParcelContentProvider pcp = new ParcelContentProvider();
								while ((nextLine = reader.readNext()) != null)
									if (nextLine.length == 2)
										pcp.add(nextLine[0], nextLine[1]);
								pcp.done();
								IViewPart findView = HandlerUtil
										.getActiveWorkbenchWindow(event)
										.getActivePage()
										.findView(ParcelView.ID);
								ParcelView parcelView = (ParcelView) findView;
								parcelView.loadGraphWithModel(pcp);
								parcelView.setXY(x, y);

								findView = HandlerUtil
										.getActiveWorkbenchWindow(event)
										.getActivePage()
										.findView(CollusionView.ID);
								CollusionView collusionView = (CollusionView) findView;
								collusionView.clearData();
							}
						} catch (NumberFormatException | IOException e) {
							e.printStackTrace();
						}

					}
				});

				return Status.OK_STATUS;
			}
		};
		job.schedule();
		return null;
	}
}
