package pf.upf.proto.rattack.model;

public class Neighbour {
	final String label;
	final Parcel source;
	final Parcel destination;

	public Neighbour(String label, Parcel source, Parcel destination) {
		this.label = label;
		this.source = source;
		this.destination = destination;
	}

	public String getLabel() {
		return label;
	}

	public Parcel getSource() {
		return source;
	}

	public Parcel getDestination() {
		return destination;
	}

	public Parcel of(Parcel parcel) {
		if (parcel.equals(source))
			return destination;
		if (parcel.equals(destination))
			return source;
		return null;
	}

	/*@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		return result;
	}*/

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Neighbour other = (Neighbour) obj;

		if ((source.equals(other.source) && (destination
				.equals(other.destination)))
				|| (source.equals(other.destination) && (destination
						.equals(other.source))))
			return true;
		return false;
	}

}