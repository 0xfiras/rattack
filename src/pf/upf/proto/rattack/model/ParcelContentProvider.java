package pf.upf.proto.rattack.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ParcelContentProvider {
	private Set<Neighbour> connections;
	private Set<Parcel> nodes;

	public ParcelContentProvider() {
		connections = new HashSet<>();
		nodes = new HashSet<>();
	}

	public List<Parcel> getNodes() {
		List<Parcel> l = new ArrayList<Parcel>(nodes);
		return l;
	}

	public List<Neighbour> getConnections() {
		List<Neighbour> l = new ArrayList<Neighbour>(connections);
		return l;
	}

	public void add(String node1, String node2) {
		Parcel first = new Parcel(node1);
		Parcel second = new Parcel(node2);

		boolean firstClean = false, secondClean = false;

		if (!nodes.contains(first))
			firstClean = nodes.add(first);
		if (!nodes.contains(second))
			secondClean = nodes.add(second);

		while (!firstClean || !secondClean) {
			for (Parcel p : nodes) {
				if (!firstClean && p.equals(first)) {
					first = p;
					firstClean = true;
				} else if (!secondClean && p.equals(second)) {
					second = p;
					secondClean = true;
				}
			}
		}

		Neighbour neighbour = new Neighbour(node1 + " - " + node2, first,
				second);
		if (!connections.contains(neighbour))
			connections.add(neighbour);

	}

	public void done() {
		for (Neighbour n : connections) {
			n.getSource().assignNeighbour(n);
			n.getDestination().assignNeighbour(n);
		}

		for (Parcel p : nodes) {
			p.calculateSignature();
		}
	}

}
