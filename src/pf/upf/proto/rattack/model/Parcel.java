package pf.upf.proto.rattack.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pf.upf.proto.rattack.views.CollusionView;

public class Parcel extends AModel {

	public int X, Y;
	private String name;
	private Set<Neighbour> neighbours;
	private Set<String> signature;
	private Map<String, Set<String>> usersToNetworkSelectionList;

	public Parcel(String name) {
		this.X = this.Y = -1;
		this.name = name;
		this.usersToNetworkSelectionList = new HashMap<>();
		this.neighbours = new HashSet<>();
	}

	public boolean isSelected(String user) {
		if (this.usersToNetworkSelectionList.containsKey(user))
			return this.usersToNetworkSelectionList.get(user).contains(
					this.name);
		else
			return false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Neighbour> getNeighbours() {
		return neighbours;
	}

	public Map<String, Set<String>> getUsersToNetworkSelectionList() {
		return usersToNetworkSelectionList;
	}

	public void assignNeighbour(Neighbour neighbour) {
		if (this.neighbours == null)
			this.neighbours = new HashSet<>();
		if (!this.neighbours.contains(neighbour))
			this.neighbours.add(neighbour);
	}

	public Map<Set<String>, Set<Set<String>>> select(String user, int x, int y,
			List<String> failureReason) {
		Map<Set<String>, Set<Set<String>>> returnMap = CollusionView
				.collusionsOfUser(user);
		if (this.isAvailable(user, failureReason)
				&& this.isUnderXYCollusionResistance(returnMap, user, x, y,
						true, failureReason)) {
			if (!this.usersToNetworkSelectionList.containsKey(user)) {
				Set<String> networkSelectionList = new HashSet<>();
				networkSelectionList.add(this.name);
				this.usersToNetworkSelectionList
						.put(user, networkSelectionList);
			} else {
				this.usersToNetworkSelectionList.get(user).add(this.name);
			}

			// this.firePropertyChange("selectedFromNetwork", clone,
			// this.selectedFromNetwork);
			for (Neighbour neighbour : neighbours) {
				if (!neighbour.of(this).usersToNetworkSelectionList
						.containsKey(user)) {
					Set<String> networkSelectionList = new HashSet<>();
					networkSelectionList.add(this.name);
					neighbour.of(this).usersToNetworkSelectionList.put(user,
							networkSelectionList);
				}
				neighbour.of(this).usersToNetworkSelectionList.get(user).add(
						this.name);
				// neighbour.of(this).firePropertyChange("selectedFromNetwork",
				// clone, neighbour.of(this).selectedFromNetwork);
			}
			return returnMap;
		}
		return null;

	}

	public boolean isUnderXYCollusionResistance(
			Map<Set<String>, Set<Set<String>>> returnMap, String user, int x,
			int y, boolean isDirectlyQueried, List<String> failureReason) {
		if (x < 2 || y < 1)
			return true;

		Set<String> usersList = new HashSet<>(
				this.usersToNetworkSelectionList.keySet());
		usersList.add(user);
		Set<Set<String>> combinations = combinations(usersList, x);
		for (Set<String> combination : combinations) {
			if (!combination.contains(user))
				continue;
			Set<String> expectedTotalSelection = new HashSet<>();

			for (String otherUser : combination)
				if (this.usersToNetworkSelectionList.containsKey(otherUser))
					expectedTotalSelection
							.addAll(this.usersToNetworkSelectionList
									.get(otherUser));
			expectedTotalSelection.add(this.name);
			if (this.signature.equals(expectedTotalSelection)) {
				Set<String> query = new HashSet<>();
				query.addAll(combination);
				query.add(user);
				if (returnMap.containsKey(query)) {
					// Why? Because if it passed then of course it is a
					// permissible collusion that happened earlier in
					// history
					if (returnMap.get(query).contains(this.signature))
						continue;
					if (returnMap.get(query).size() < y - 2) {
						returnMap.get(query).add(this.signature);
						continue;
					} else {
						failureReason.add("Collusion by " + query + " on "
								+ this.signature
								+ ", while already colliding with: "
								+ returnMap.get(query));
						return false;
					}
				} else {
					if (y == 1)
						return false;
					Set<Set<String>> newCollusion = new HashSet<>();
					newCollusion.add(this.signature);
					returnMap.put(query, newCollusion);
					continue;
				}
			}
		}

		boolean continueProbingNeighbours = true;
		if (isDirectlyQueried) {
			for (Neighbour n : this.neighbours) {
				continueProbingNeighbours = n.of(this)
						.isUnderXYCollusionResistance(returnMap, user, x, y,
								false, failureReason);
				if (!continueProbingNeighbours)
					break;
			}
		}

		// repeat this for neighbouring parcels

		return continueProbingNeighbours;
	}

	public Set<Set<String>> combinations(Set<String> set, int power) {
		List<Set<String>> array = new ArrayList<Set<String>>();
		for (int i = 0; i < power; i++)
			array.add(new HashSet<>(set));
		return cartesianProduct(array);
	}

	public Set<Set<String>> cartesianProduct(List<Set<String>> sets) {
		if (sets.size() < 2)
			throw new IllegalArgumentException(
					"Can't have a product of fewer than two sets (got "
							+ sets.size() + ")");
		Set<Set<String>> temp = _cartesianProduct(0, sets);
		Set<Set<String>> ret = new HashSet<>();
		for (Set<String> set : temp)
			if (set.size() == sets.size())
				ret.add(set);
		return ret;
	}

	private Set<Set<String>> _cartesianProduct(int index, List<Set<String>> sets) {
		Set<Set<String>> ret = new HashSet<Set<String>>();
		if (index == sets.size()) {
			ret.add(new HashSet<String>());
		} else {
			for (String obj : sets.get(index)) {
				for (Set<String> set : _cartesianProduct(index + 1, sets)) {
					set.add(obj);
					ret.add(set);
				}
			}
		}
		return ret;
	}

	private boolean isAvailable(String user,List<String> failureReason) {
		if (isSelected(user)) {
			failureReason.add(user + " already selected " + this.name);
			return false;
		}

		boolean canSelect = true;
		if (!this.isFull(user, failureReason)) {
			for (Neighbour neighbour : neighbours) {
				if (neighbour.of(this).isFull(user, failureReason)) {
					canSelect = false;
					break;
				}
			}
		} else {
			canSelect = false;
		}
		return canSelect;
	}

	private boolean isFull(String user, List<String> failureReason) {
		if (this.usersToNetworkSelectionList.containsKey(user)) {
			if (this.usersToNetworkSelectionList.get(user).size() == neighbours
					.size()) {
				failureReason.add(user + " has maximized " + this.signature);
				return true;
			}
		}
		return false;
	}

	public boolean hasNeighbourRelationship(Parcel p2) {
		for (Neighbour neighbour : neighbours)
			if (p2.equals(neighbour.of(this)))
				return true;
		return false;
	}

	public Neighbour getNeighbourRelationship(Parcel p2) {
		for (Neighbour neighbour : neighbours)
			if (p2.equals(neighbour.of(this)))
				return neighbour;
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Parcel other = (Parcel) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void calculateSignature() {
		signature = new HashSet<>();
		signature.add(name);
		for (Neighbour n : neighbours) {
			signature.add(n.of(this).name);
		}
	}

	public Set<String> getSignature() {
		return this.signature;
	}

}
