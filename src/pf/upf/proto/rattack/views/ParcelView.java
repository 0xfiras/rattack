package pf.upf.proto.rattack.views;

import org.eclipse.jface.action.ControlContribution;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.zest.core.viewers.AbstractZoomableViewer;
import org.eclipse.zest.core.viewers.IZoomableWorkbenchPart;
import org.eclipse.zest.core.viewers.ZoomContributionViewItem;
import org.eclipse.zest.layouts.LayoutAlgorithm;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;

import pf.upf.proto.rattack.model.ParcelContentProvider;
import pf.upf.proto.rattack.providers.ParcelGraphLabelProvider;
import pf.upf.proto.rattack.providers.ParcelGraphContentProvider;

public class ParcelView extends ViewPart implements IZoomableWorkbenchPart {

	public static final String ID = "pf.upf.proto.rattack.views.ParcelView";
	private ParcelViewGraph viewer;

	Text currentUserText;

	public void createPartControl(Composite parent) {
		viewer = new ParcelViewGraph(parent, SWT.BORDER);
		viewer.setContentProvider(new ParcelGraphContentProvider());
		viewer.setLabelProvider(new ParcelGraphLabelProvider());
		ParcelContentProvider model = new ParcelContentProvider();
		loadGraphWithModel(model);
		fillToolBar();
		setupAntialias();
	}

	public void loadGraphWithModel(ParcelContentProvider model) {
		viewer.setInput(model.getNodes());
		LayoutAlgorithm layout = setLayout();
		viewer.setLayoutAlgorithm(layout, true);
		viewer.applyLayout();
	}

	private void setupAntialias() {
		Listener[] listeners = viewer.getControl().getListeners(SWT.Paint);
		for (int i = 0; i < listeners.length; i++) {
			viewer.getControl().removeListener(SWT.Paint, listeners[i]);
		}

		viewer.getControl().addListener(SWT.Paint, new Listener() {
			@Override
			public void handleEvent(Event event) {
				event.gc.setAntialias(SWT.ON);
				event.gc.setTextAntialias(SWT.ON);
			}
		});

		for (int i = 0; i < listeners.length; i++) {
			viewer.getControl().addListener(SWT.Paint, listeners[i]);
		}
	}

	private LayoutAlgorithm setLayout() {
		LayoutAlgorithm layout;
		layout = new SpringLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		// layout = new
		// TreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		// layout = new
		// GridLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		// layout = new
		// HorizontalTreeLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		// layout = new
		// RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING);
		return layout;

	}

	/**
	 * Passing the focus request to the viewer's control.
	 */

	public void setFocus() {
	}

	private void fillToolBar() {
		ZoomContributionViewItem toolbarZoomContributionViewItem = new ZoomContributionViewItem(
				this);
		IActionBars bars = getViewSite().getActionBars();
		bars.getMenuManager().add(toolbarZoomContributionViewItem);
		bars.getToolBarManager().add(new ControlContribution("search") {

			@Override
			protected Control createControl(Composite parent) {
				Label lbl = new Label(parent, SWT.BORDER);
				lbl.setText("Current User");
				return lbl;
			}

		});
		bars.getToolBarManager().add(new ControlContribution("search") {

			@Override
			protected Control createControl(Composite parent) {
				currentUserText = new Text(parent, SWT.BORDER);
				viewer.setCurrentUserText(currentUserText);
				return currentUserText;
			}

		});
	}

	@Override
	public AbstractZoomableViewer getZoomableViewer() {
		return viewer;
	}

	public void setXY(int x, int y) {
		this.viewer.setXY(x, y);		
	}
}
