package pf.upf.proto.rattack.views;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.ViewPart;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

import pf.upf.proto.rattack.providers.ParcelPropertyLabelProvider;
import pf.upf.proto.rattack.providers.ParcelPropertyModelProvider;

public class ParcelPropertyView extends ViewPart {

	public static final String ID = "pf.upf.proto.rattack.views.ParcelPropertyView"; //$NON-NLS-1$
	private TableViewer tableViewer;
	private TableColumn tblclmnConsumed;
	private Table table;

	public ParcelPropertyView() {

	}

	public void setUserSelectionList(
			final Map<String, Set<String>> userSelectionList) {
		this.tableViewer.setInput(userSelectionList);
	}

	public void setSignature(Set<String> signature) {
		tblclmnConsumed.setText("Consumed from " + signature);
	}

	/**
	 * Create contents of the view part.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(final Composite parent) {
		// Composite container = new Composite(parent, SWT.NONE);
		// container.setLayout(new FillLayout(SWT.HORIZONTAL));

		Composite composite = new Composite(parent, SWT.NONE);
		TableColumnLayout tcl_composite = new TableColumnLayout();
		composite.setLayout(tcl_composite);

		this.tableViewer = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		this.tableViewer.setContentProvider(new ParcelPropertyModelProvider());
		this.tableViewer.setLabelProvider(new ParcelPropertyLabelProvider());
		table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableViewerColumn tableViewerColumn = new TableViewerColumn(
				tableViewer, SWT.NONE);
		TableColumn tblclmnUser = tableViewerColumn.getColumn();
		tcl_composite.setColumnData(tblclmnUser, new ColumnPixelData(150, true,
				true));
		tblclmnUser.setText("User");
		tableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@SuppressWarnings("unchecked")
			@Override
			public String getText(Object element) {
				if (element instanceof Entry)
					return ((Entry<String, Set<String>>) element).getKey();
				return "";
			}
		});

		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(
				tableViewer, SWT.NONE);
		tblclmnConsumed = tableViewerColumn_1.getColumn();
		tcl_composite.setColumnData(tblclmnConsumed, new ColumnPixelData(150,
				true, true));
		tblclmnConsumed.setText("Consumed");
		tableViewerColumn_1.setLabelProvider(new ColumnLabelProvider() {
			@SuppressWarnings("unchecked")
			@Override
			public String getText(Object element) {
				if (element instanceof Entry)
					return ((Entry<String, Set<String>>) element).getValue()
							.toString();
				return "";
			}
		});

		BundleContext ctx = FrameworkUtil.getBundle(ParcelPropertyView.class)
				.getBundleContext();
		EventHandler handler = new EventHandler() {
			public void handleEvent(final Event event) {
				if (parent.getDisplay().getThread() == Thread.currentThread()) {
					tableViewer.setInput(event.getProperty("DATA"));
					tblclmnConsumed.setText("Consumed from "
							+ event.getProperty("SIGNATURE"));
				} else {
					parent.getDisplay().syncExec(new Runnable() {
						public void run() {
							tableViewer.setInput(event.getProperty("DATA"));
							tblclmnConsumed.setText("Consumed from "
									+ event.getProperty("SIGNATURE"));
						}
					});
				}
			}
		};

		Dictionary<String, String> properties = new Hashtable<String, String>();
		properties.put(EventConstants.EVENT_TOPIC,
				"parcelviewgraph/nodeSelection");
		ctx.registerService(EventHandler.class, handler, properties);

		createActions();
		initializeToolBar();
		initializeMenu();
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
		// IToolBarManager toolbarManager = getViewSite().getActionBars()
		// .getToolBarManager();
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
		// IMenuManager menuManager = getViewSite().getActionBars()
		// .getMenuManager();
	}

	@Override
	public void setFocus() {
		// Set the focus
	}
}
