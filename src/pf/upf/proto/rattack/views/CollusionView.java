package pf.upf.proto.rattack.views;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.ViewPart;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;

import pf.upf.proto.rattack.providers.CollusionLabelProvider;
import pf.upf.proto.rattack.providers.CollusionModelProvider;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.widgets.Text;

public class CollusionView extends ViewPart {

	public static final String ID = "pf.upf.proto.rattack.views.CollusionView"; //$NON-NLS-1$
	private TableViewer tableViewer;
	private TableColumn tblclmnCollusions;
	public static Map<Set<String>, Set<Set<String>>> COLLUSION_MAP;
	private Text failureText;

	public CollusionView() {
		COLLUSION_MAP = new HashMap<>();
	}

	/**
	 * Create contents of the view part.
	 * 
	 * @param parent
	 */
	@Override
	public void createPartControl(final Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		TableColumnLayout tcl_composite = new TableColumnLayout();
		composite.setLayout(tcl_composite);

		tableViewer = new TableViewer(composite, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		Table table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		// Table table = tableViewer.getTable();
		this.tableViewer.setContentProvider(new CollusionModelProvider());
		this.tableViewer.setLabelProvider(new CollusionLabelProvider());

		TableViewerColumn tableViewerColumn = new TableViewerColumn(
				tableViewer, SWT.NONE);
		TableColumn tblclmnUsers = tableViewerColumn.getColumn();
		tcl_composite.setColumnData(tblclmnUsers, new ColumnPixelData(150,
				true, true));
		tblclmnUsers.setWidth(100);
		tblclmnUsers.setText("Users");
		tableViewerColumn.setLabelProvider(new ColumnLabelProvider() {
			@SuppressWarnings("unchecked")
			@Override
			public String getText(Object element) {
				if (element instanceof Entry)
					return ((Entry<Set<String>, Set<Set<String>>>) element)
							.getKey().toString();
				return "";
			}
		});

		TableViewerColumn tableViewerColumn_1 = new TableViewerColumn(
				tableViewer, SWT.NONE);
		tblclmnCollusions = tableViewerColumn_1.getColumn();
		tcl_composite.setColumnData(tblclmnCollusions, new ColumnPixelData(150,
				true, true));
		tblclmnCollusions.setWidth(100);
		tblclmnCollusions.setText("Collusions");
		tableViewerColumn_1.setLabelProvider(new ColumnLabelProvider() {
			@SuppressWarnings("unchecked")
			@Override
			public String getText(Object element) {
				if (element instanceof Entry)
					return ((Entry<String, Set<Set<String>>>) element)
							.getValue().toString();
				return "";
			}
		});

		SashForm sashForm = new SashForm(parent, SWT.NONE);

		failureText = new Text(sashForm, SWT.BORDER | SWT.READ_ONLY
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		sashForm.setWeights(new int[] { 1 });

		BundleContext ctx = FrameworkUtil.getBundle(ParcelPropertyView.class)
				.getBundleContext();
		EventHandler newCollusionsHandler = new EventHandler() {
			@SuppressWarnings("unchecked")
			public void handleEvent(final Event event) {
				if (parent.getDisplay().getThread() == Thread.currentThread()) {
					refillTable((Map<Set<String>, Set<Set<String>>>) event
							.getProperty("DATA"));
				} else {
					parent.getDisplay().syncExec(new Runnable() {
						public void run() {
							refillTable((Map<Set<String>, Set<Set<String>>>) event
									.getProperty("DATA"));
						}
					});
				}
			}
		};

		Dictionary<String, String> collusionEvtProps = new Hashtable<String, String>();
		collusionEvtProps.put(EventConstants.EVENT_TOPIC,
				"parcelviewgraph/newCollusions");
		ctx.registerService(EventHandler.class, newCollusionsHandler,
				collusionEvtProps);

		EventHandler failureReasonHandler = new EventHandler() {
			@SuppressWarnings("unchecked")
			public void handleEvent(final Event event) {
				if (parent.getDisplay().getThread() == Thread.currentThread()) {
					addFailureReason((List<String>) event.getProperty("DATA"));
				} else {
					parent.getDisplay().syncExec(new Runnable() {
						public void run() {
							addFailureReason((List<String>) event
									.getProperty("DATA"));
						}
					});
				}
			}
		};

		Dictionary<String, String> failureEvtProps = new Hashtable<String, String>();
		failureEvtProps.put(EventConstants.EVENT_TOPIC,
				"parcelviewgraph/failureReason");
		ctx.registerService(EventHandler.class, failureReasonHandler,
				failureEvtProps);

		createActions();
		initializeToolBar();
		initializeMenu();
	}

	protected void addFailureReason(List<String> reasons) {
		String ret = "";
		for (String reason : reasons)
			ret = ret.concat(reason.concat("\r\n"));
		this.failureText.setText(ret.concat(this.failureText.getText()));

	}

	private void refillTable(Map<Set<String>, Set<Set<String>>> map) {
		if (map == null || map.isEmpty())
			return;
		COLLUSION_MAP.putAll(map);
		tableViewer.setInput(COLLUSION_MAP);
	}

	public static Map<Set<String>, Set<Set<String>>> collusionsOfUser(
			String userName) {
		Map<Set<String>, Set<Set<String>>> returnMap = new HashMap<>();
		for (Entry<Set<String>, Set<Set<String>>> colluders : COLLUSION_MAP
				.entrySet()) {
			if (colluders.getKey().contains(userName)) {
				returnMap.put(new HashSet<>(colluders.getKey()), new HashSet<>(
						colluders.getValue()));
			}
		}
		return returnMap;
	}

	/**
	 * Create the actions.
	 */
	private void createActions() {
		// Create the actions
	}

	/**
	 * Initialize the toolbar.
	 */
	private void initializeToolBar() {
	}

	/**
	 * Initialize the menu.
	 */
	private void initializeMenu() {
	}

	@Override
	public void setFocus() {
		// Set the focus
	}

	public void clearData() {
		COLLUSION_MAP.clear();
		this.tableViewer.setInput(COLLUSION_MAP);
	}
}
