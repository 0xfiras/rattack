package pf.upf.proto.rattack.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.zest.core.viewers.GraphViewer;
import org.eclipse.zest.core.widgets.GraphNode;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.event.EventAdmin;

import pf.upf.proto.rattack.model.Parcel;

public class ParcelViewGraph extends GraphViewer implements KeyListener {

	private boolean isFirstCall = true;
	private int x, y;
	private Text currentUserText;

	private BundleContext ctx;
	private EventAdmin eventAdmin;

	public ParcelViewGraph(Composite composite, int style) {
		super(composite, style);
		this.graph.addKeyListener(this);
		this.graph.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (e.item instanceof GraphNode) {
					Parcel p = (Parcel) ((GraphNode) e.item).getData();
					postNodeSelectionEvent(p.getUsersToNetworkSelectionList(),
							p.getSignature());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.character == 's') {
			for (Object o : this.graph.getSelection()) {
				if (o instanceof GraphNode) {
					Parcel p = (Parcel) ((GraphNode) o).getData();
					if (this.currentUserText.getText().trim().isEmpty())
						return;
					List<String> failureReason = new ArrayList<>();
					Map<Set<String>, Set<Set<String>>> collusionResult = p
							.select(this.currentUserText.getText().trim(), x, y, failureReason);
					
					if (collusionResult != null)
						postNewCollusionsEvent(collusionResult);
					
					if(failureReason != null && !failureReason.isEmpty())
						postFailureReasonEvent(failureReason);
					postNodeSelectionEvent(p.getUsersToNetworkSelectionList(),
							p.getSignature());
				}
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	private void postNodeSelectionEvent(Map<String, Set<String>> map,
			Set<String> signature) {
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("DATA", map);
		properties.put("SIGNATURE", signature);
		postEvent("parcelviewgraph/nodeSelection", properties);
	}

	private void postNewCollusionsEvent(Map<Set<String>, Set<Set<String>>> map) {
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("DATA", map);
		postEvent("parcelviewgraph/newCollusions", properties);
	}

	public void postFailureReasonEvent(List<String> reason) {
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("DATA", reason);
		postEvent("parcelviewgraph/failureReason", properties);
	}

	private void postEvent(String eventPath, Map<String, Object> props) {
		if (isFirstCall) {
			ctx = FrameworkUtil.getBundle(ParcelView.class).getBundleContext();
			ServiceReference<EventAdmin> ref = ctx
					.getServiceReference(EventAdmin.class);
			eventAdmin = ctx.getService(ref);
			isFirstCall = false;
		}
		org.osgi.service.event.Event event = new org.osgi.service.event.Event(
				eventPath, props);
		eventAdmin.postEvent(event);
	}

	public void setCurrentUserText(Text currentUserText) {
		this.currentUserText = currentUserText;

	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

}
